<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\EricController;
use App\Http\Controllers\MailController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/eric', [EricController::class, 'show']);

Route::get('/ericDirecte', function(){
    return view('ericDirecte');
});

Route::get('/send/email', [MailController::class, 'mail']);

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';
